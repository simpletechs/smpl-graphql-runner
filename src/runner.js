'use strict'

const requestPromise = require('request-promise')
const log = require('debug')('smpl-graphql-runner:graphql:runner')

class GraphQLRunner {
    constructor(endPoint, requestFn) {
        this.endPoint = endPoint || process.env.SMPL_DATA_API_URL
        log('setting endpoint to', this.endPoint)
        if(typeof this.endPoint !== 'string' || !this.endPoint.length) {
            throw new Error('did not provide GraphQL Endpoint to the SMPL GraphQLRunner instance. Provide one or set the environment variable "SMPL_DATA_API_URL".')
        }
        if(requestFn && !typeof requestFn === 'function') {
            throw new Error('invalid request function provided')
        }
        this.requestFn = requestFn || requestPromise
    }

    async runQuery (query, variables = 'null') {
        const response = await this.requestFn(this.endPoint, {
            method: 'POST',
            json: true,
            body: {
                query,
                variables
            }
        })

        const { errors, data } = response
        if((errors && errors.length > 0) || !data ) {
            log('error while loading data from endpoint', this.endPoint, errors, data)
            throw response // throw the unaltered response to allow apps to handle every edge case correctly
        }
        return response
    }
}

module.exports = {
    GraphQLRunner
}