const { expect } = require('chai')
const proxyquire = require('proxyquire')
const spy = require('sinon').spy()

const pkg = require('../package.json')
const mainPath = '../'+pkg.main
const { GraphQLRunner } = require(mainPath)
let runnerProxyFailing // holds our proxyquired runner (refreshed for each test)
let runnerProxyData

describe('GraphQLRunner', () => {


    before(() => {
        expect(process.env).to.have.property('NODE_ENV', 'test') // just to make sure
        // process.env.SMPL_SERVICE_AUTH_API_URL = config.SMPL_SERVICE_AUTH_API_URL // request-promise module is directly using process.env
    })

    beforeEach(() => {
        // re-proxyquire to reset internal state for each test (else we would not run into refresh function)
        spy.reset()
        runnerProxyData = proxyquire(mainPath, {
            'request-promise': async function() {
                spy.apply(this, arguments)
                return { data: { someData: true }, errors: null }
            }
        })
    })

    it('should provide class and allow runQuery', () => {
        expect(GraphQLRunner, 'constructor').to.exist
        const instance = new GraphQLRunner('test')
        expect(instance, 'instance').to.exist.with.property('runQuery').to.be.a('function')
    })

    it('should respect environment variable SMPL_DATA_API_URL', async () => {
        const { SMPL_DATA_API_URL: oldValue } = process.env
        try {
            process.env.SMPL_DATA_API_URL = 'MOCKED'
            const { GraphQLRunner } = runnerProxyData
            const runner = new GraphQLRunner()
            await runner.runQuery('some query', {})
            expect(spy.calledOnce).to.be.true
            expect(spy.getCall(0).args[0]).to.equal('MOCKED')
        } catch (e) {
            process.env.SMPL_DATA_API_URL = oldValue
            throw e
        }
    })


    it('should throw error if there is no endpoint provided', () => {
        const { SMPL_DATA_API_URL: oldValue } = process.env
        try {
            process.env.SMPL_DATA_API_URL = undefined
            try {
                new GraphQLRunner()
                expect(false).to.equal(false, 'this should never be reached')            
            } catch (e) {
                expect(e).to.exist;
            }
        } catch (e) {
            process.env.SMPL_DATA_API_URL = oldValue
            throw e
        }
    })

    it('should provide class and allow runQuery with proxyquire', () => {
        const { GraphQLRunner } = runnerProxyData
        expect(GraphQLRunner, 'constructor').to.exist
        const instance = new GraphQLRunner('test')
        expect(instance, 'instance').to.exist.with.property('runQuery').to.be.a('function')
    })

    it('should respect endpoint', async () => {
        const runner = new runnerProxyData.GraphQLRunner('test')

        await runner.runQuery('some query', {})
        expect(spy.calledOnce).to.be.true
        expect(spy.getCall(0).args[0]).to.equal('test')
    })

    it('should return data', async () => {
        const runner = new runnerProxyData.GraphQLRunner('test')

        const res = await runner.runQuery('some query', {})
        expect(res).to.exist.and.to.have.property('data').with.property('someData', true)
    })

    it('should throw if there is an error', async () => {
        const runnerProxyFailing = proxyquire(mainPath, {
            'request-promise': async function() {
                spy.apply(this, arguments)
                return { data: null, errors: ['MOCKED']}
            }
        })
        const runner = new runnerProxyFailing.GraphQLRunner('test')
        try {
            await runner.runQuery('some query', {})            
            expect(false).to.equal(false, 'this should never be reached')
        } catch (e) {
            expect(e).to.exist.with.property('errors').to.be.an('array');
            expect(e.errors).to.eql(['MOCKED'])
        }
    })

    it('should throw if there is no data', async () => {
        const runnerProxyFailing = proxyquire(mainPath, {
            'request-promise': async function() {
                spy.apply(this, arguments)
                return { data: null, errors: null }
            }
        })
        const runner = new runnerProxyFailing.GraphQLRunner('test')
        try {
            await runner.runQuery('some query', {})            
            expect(false).to.equal(false, 'this should never be reached')
        } catch (e) {
            expect(e).to.exist.with.property('errors', null);
            expect(e).to.have.property('data', null)
        }
    })
})