# SMPL GraphQL Runner
A very smpl wrapper around http requests to a GraphQL endpoint. Throws if a GraphQL response returned an error.
It can use custom request functions that are compatible to [request-promise](https://github.com/request/request-promise), such as [smpl-authed-request](https://bitbucket.org/simpletechs/smpl-authed-request/).